package com.xenoterracide.entities.api;

public interface Typed<T extends Enum<T>> {
    T getType();
}
