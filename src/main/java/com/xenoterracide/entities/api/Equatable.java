package com.xenoterracide.entities.api;

public interface Equatable {

    default boolean isEqualTo( Object o ) {
        if ( o == this ) {
            return true;
        }
        if ( o instanceof Equatable ) {
            Equatable that = (Equatable) o;
            return that.canEquals( this ) && this.canEquals( that );
        }
        return false;
    }

    boolean canEquals( Equatable o );
}
