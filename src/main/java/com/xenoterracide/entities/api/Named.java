package com.xenoterracide.entities.api;

public interface Named {
    String getName();
}
