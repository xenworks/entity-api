package com.xenoterracide.entities.api;

import java.time.Instant;
import java.util.Optional;

public interface CreatedOn {

    Optional<Instant> getCreatedOn();
}
