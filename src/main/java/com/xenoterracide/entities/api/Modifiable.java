package com.xenoterracide.entities.api;

import java.time.Instant;
import java.util.NavigableSet;
import java.util.Optional;

public interface Modifiable {

    default Optional<Instant> getLastModifiedTime() {
        return Optional.ofNullable( this.getModifiedOn().last() );
    }

    NavigableSet<Instant> getModifiedOn();
}
