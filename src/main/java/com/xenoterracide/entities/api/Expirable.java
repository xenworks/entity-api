package com.xenoterracide.entities.api;

import java.time.Instant;
import java.util.Optional;

public interface Expirable {
    default Boolean isExpired() {
        return this.getExpiration()
                .map( ( expiration ) -> Instant.now().isAfter( expiration ) )
                .orElse( false );
    }

    Optional<Instant> getExpiration();
}
