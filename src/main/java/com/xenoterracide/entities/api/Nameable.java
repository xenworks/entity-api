package com.xenoterracide.entities.api;

public interface Nameable {
    void setName( String name );
}
