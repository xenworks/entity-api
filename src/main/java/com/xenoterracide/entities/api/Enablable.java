package com.xenoterracide.entities.api;

public interface Enablable {
    Boolean isEnabled();
}
