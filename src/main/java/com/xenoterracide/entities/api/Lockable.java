package com.xenoterracide.entities.api;

import java.time.Instant;
import java.util.Optional;

public interface Lockable {

    default Boolean isLocked() {
        return this.isLockedOn( Instant.now() );
    }

    default Boolean isLockedOn( Instant instant ) {
        boolean after;
        try {
            after = this.itIsAfterLockedOn( instant );
        }
        catch ( IllegalStateException e ) {
            return false;
        }

        try {
            return after && this.itIsBeforeLockedUntil( instant );
        }
        catch ( IllegalStateException e ) {
            return true;
        }
    }

    default Boolean itIsAfterLockedOn( Instant instant ) throws IllegalStateException {
        return this.getLockedOn().map( instant::isAfter )
                .orElseThrow( () -> new IllegalStateException( "no instant" ) );
    }

    default Boolean itIsBeforeLockedUntil( Instant instant ) throws IllegalStateException {
        return this.getLockedUntil().map( instant::isBefore )
                .orElseThrow( () -> new IllegalStateException( "no instant" ) );
    }

    Optional<Instant> getLockedOn();

    Optional<Instant> getLockedUntil();
}
