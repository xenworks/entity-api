package com.xenoterracide.entities.api

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.domain.Persistable
import java.io.Serializable


interface Identified<ID : Serializable> : Persistable<ID> {

    @JsonIgnore
    override fun isNew(): Boolean {
        return this.id == null
    }

    fun sameIdAs(that: Identified<ID>): Boolean {
        return this.id == that.id
    }
}
