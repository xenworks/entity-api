package com.xenoterracide.entities.api

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.data.domain.Persistable


class IdentifiedTest {

    @Test
    fun testIsNew() {
        val e0: Persistable<Long> = Entity(1L)
        val e1: Persistable<Long> = Entity(null)

        assertThat(e0.isNew).`as`("%s is not new", e0).isFalse()
        assertThat(e1.isNew).`as`("%s is new", e1).isTrue()
    }

    @Test
    fun sameIdAs() {
        val e0: Identified<Long> = Entity(1L)
        val e1: Identified<Long> = Entity(null)
        val e2: Identified<Long> = Entity(1L)
        val e3: Identified<Long> = Entity(3L)

        assertThat(e0.sameIdAs(e0)).`as`("%s has same identity as self %s", e0, e0).isTrue()
        assertThat(e0.sameIdAs(e1)).`as`("%s has same identity as %s", e0, e1).isFalse()
        assertThat(e0.sameIdAs(e2)).`as`("%s has same identity as %s", e0, e2).isTrue()
        assertThat(e0.sameIdAs(e3)).`as`("%s has same identity as %s", e0, e3).isFalse()
    }

    private data class Entity(private var _id: Long?) : Identified<Long> {
        override fun getId(): Long? {
            return _id
        }
    }
}
