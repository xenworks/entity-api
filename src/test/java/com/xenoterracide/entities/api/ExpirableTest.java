package com.xenoterracide.entities.api;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class ExpirableTest {

    @Test
    void testIsExpired() {
        Future future = new Future();
        Past past = new Past();
        NeverSet neverSet = new NeverSet();

        assertThat( future.isExpired() ).as( "future" ).isFalse();
        assertThat( past.isExpired() ).as( "past" ).isTrue();
        assertThat( neverSet.isExpired() ).as( "not set" ).isFalse();
    }

    private static class NeverSet implements Expirable {
        @Override
        public Optional<Instant> getExpiration() {
            return Optional.empty();
        }
    }

    private static class Future implements Expirable {
        @Override
        public Optional<Instant> getExpiration() {
            return Optional.of( Instant.now().plus( 1, ChronoUnit.MINUTES ) );
        }
    }

    private static class Past implements Expirable {

        @Override
        public Optional<Instant> getExpiration() {
            return Optional.of( Instant.now().minus( 1, ChronoUnit.MINUTES ) );
        }
    }
}
