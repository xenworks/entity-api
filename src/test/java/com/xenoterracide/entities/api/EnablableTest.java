package com.xenoterracide.entities.api;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class EnablableTest {

    @Test
    void testEnabled() {
        Enablable mock = mock( Enablable.class );

        assertThat( mock ).isInstanceOf( Enablable.class );
        assertThat( mock.isEnabled() ).isFalse();
    }

}
