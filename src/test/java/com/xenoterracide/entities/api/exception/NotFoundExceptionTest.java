package com.xenoterracide.entities.api.exception;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class NotFoundExceptionTest {

    @Test
    void testNotFound() {
        String message = "not found";

        NotFoundException e = assertThrows( NotFoundException.class, () -> {
            throw new NotFoundException( "com/xenoterracide/entity/test" ) {
                @Override
                public String getMessage() {
                    return message;
                }
            };
        } );

        assertThat( e ).hasMessage( message );
    }
}
