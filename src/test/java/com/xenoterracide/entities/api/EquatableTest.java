package com.xenoterracide.entities.api;

import java.util.Objects;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;


public class EquatableTest {

    @Test
    public void testIsEqualTo() {
        EqualsVerifier.forClass( Equivalent.class ).verify();
    }

    private static class Equivalent implements Equatable {
        private final Long id;

        Equivalent( Long id ) {
            this.id = id;
        }

        @Override
        public final int hashCode() {
            return Objects.hash( this.id );
        }

        @Override
        public final boolean equals( Object obj ) {
            return this.isEqualTo( obj );
        }

        @Override
        public String toString() {
            return super.toString();
        }

        @Override
        public boolean canEquals( Equatable o ) {
            if ( o instanceof Equivalent ) {
                Equivalent that = (Equivalent) o;
                return Objects.equals( this.id, that.id );
            }
            return false;
        }
    }
}
