package com.xenoterracide.entities.api;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


class LockableTest {

    private static final Duration SEC = Duration.of( 1, ChronoUnit.SECONDS );
    private static final Duration HOUR = Duration.of( 1, ChronoUnit.HOURS );

    private final LockedRange lockedRange = new LockedRange();


    @ParameterizedTest
    @ArgumentsSource( Lockables.class )
    void testIsLocked( Lockable lockable, boolean expected ) {
        assertThat( lockable.isLocked() ).isEqualTo( expected );
    }

    @ParameterizedTest( name = "[{index}] {1}  is {2} from now {0}" )
    @ArgumentsSource( Instants.class )
    void testIsLockedOn( Instant now, Instant time, Duration duration ) {
        assertThat( lockedRange.isLockedOn( time ) ).isEqualTo( SEC.equals( duration ) );

    }


    @ParameterizedTest
    @ArgumentsSource( Instants.class )
    void testItIsAfterLockedOn( Instant now, Instant time ) {
        assertThat( lockedRange.itIsAfterLockedOn( time ) ).isEqualTo( time.plusMillis( 1500 ).isAfter( now ) );
    }

    @ParameterizedTest
    @ArgumentsSource( Instants.class )
    void testItIsBeforeLockedUntil( Instant now, Instant time ) {
        assertThat( lockedRange.itIsBeforeLockedUntil( time ) ).isEqualTo( time.minusMillis( 1500 ).isBefore( now ) );
    }


    @Test
    void isBeforeOrAfterException() {
        Instant time = Instant.now();
        EmptyRange emptyRange = new EmptyRange();
        assertThrows( IllegalStateException.class, () -> emptyRange.itIsAfterLockedOn( time ) );
        assertThrows( IllegalStateException.class, () -> emptyRange.itIsBeforeLockedUntil( time ) );
    }

    @Test
    void testGetLockedOn() {
        assertThat( lockedRange.getLockedOn() ).isPresent();
    }

    @Test
    void testGetLockedUntil() {
        assertThat( lockedRange.getLockedUntil() ).isPresent();
    }

    static class Instants implements ArgumentsProvider {
        private final Instant now = Instant.now();

        @Override
        public Stream<? extends Arguments> provideArguments( ExtensionContext context ) {
            return Stream.of(
                Arguments.of( now, now.plus( SEC ), SEC ),
                Arguments.of( now, now.plus( HOUR ), HOUR ),
                Arguments.of( now, now.minus( SEC ), SEC ),
                Arguments.of( now, now.minus( HOUR ), HOUR )
            );
        }
    }

    static class Lockables implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments( ExtensionContext context ) {
            return Stream.of(
                Arguments.of( new LockedRange(), true ),
                Arguments.of( new EmptyRange(), false ),
                Arguments.of( new EmptyLockedOn(), false ),
                Arguments.of( new EmptyLockedUntil(), true )
            );
        }
    }

    private static class LockedRange implements Lockable {

        @Override
        public Optional<Instant> getLockedOn() {
            return Optional.of( Instant.now().minus( 1, ChronoUnit.MINUTES ) );
        }

        @Override
        public Optional<Instant> getLockedUntil() {
            return Optional.of( Instant.now().plus( 1, ChronoUnit.MINUTES ) );
        }
    }

    private static class EmptyRange implements Lockable {

        @Override
        public Optional<Instant> getLockedOn() {
            return Optional.empty();
        }

        @Override
        public Optional<Instant> getLockedUntil() {
            return Optional.empty();
        }
    }

    private static class EmptyLockedOn implements Lockable {

        @Override
        public Optional<Instant> getLockedOn() {
            return Optional.empty();
        }

        @Override
        public Optional<Instant> getLockedUntil() {
            return Optional.of( Instant.now().plus( 1, ChronoUnit.MINUTES ) );
        }
    }

    private static class EmptyLockedUntil implements Lockable {

        @Override
        public Optional<Instant> getLockedOn() {
            return Optional.of( Instant.now().minus( 1, ChronoUnit.MINUTES ) );
        }

        @Override
        public Optional<Instant> getLockedUntil() {
            return Optional.empty();
        }
    }
}
