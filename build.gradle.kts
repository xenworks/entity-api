plugins {
    id("com.xenoterracide.gradle.bundle.kotlin-lib-defaults").version("0.7.8")
}
group = "com.xenoterracide.entities"
version = "0.2.1-SNAPSHOT"

dependencies {
    implementation(kotlin("stdlib"))

    api("org.springframework.data:spring-data-commons")
    compileOnly("com.fasterxml.jackson.core:jackson-annotations")
    testImplementation("org.mockito:mockito-core")
    testImplementation("nl.jqno.equalsverifier:equalsverifier")

    testImplementation("org.springframework:spring-test")
    testImplementation("org.springframework.boot:spring-boot-test")
    testImplementation("org.hamcrest:hamcrest-library:1.+")
}
